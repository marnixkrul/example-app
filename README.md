# README example-app

*Created: 26 mei 2020 by Marnix Krul*

## Run the app:
```bash
yarn add
yarn start # Start the application on localhost:3000
# To run in electron you still have to start the application using the start command
yarn electron # Start the application in an electron wrapper

```

Todo: 
  1. Create Activity Page
  2. Create Notes Page

Done:
 1. Create Electron app
 2. Add config files
 3. Create Sidebar
 4. Add Navigation
 5. Add some structure / logic
 6. Add Css
