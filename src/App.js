import React, { useState } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Header from './components/Header'
import SideBar from './components/UI/SideBar'
import Card from './components/UI/Card'
import Home from './pages/Home'
import Activity from './pages/Activity'
import Notes from './pages/Notes'
import logo from './logo.svg'
import Profile from './pages/Profile'

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <SideBar />
        <main>
          <div className="container">
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/activity" component={Activity} />
              <Route path="/notes" component={Notes} />
              <Route path="/profile" component={Profile} />
            </Switch>
          </div>
        </main>
      </BrowserRouter>
      {/** Div below is a quickfix to add margin for scrollable pages */}
      <div style={{ height: '5rem' }} />
    </div>
  )
}

export default App
