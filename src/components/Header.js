import React from 'react'
import { FiMenu } from 'react-icons/fi'
import './Header.css'

function Header({ toggleSidebar, sidebar }) {
  return (
    <nav className="header-nav">
      <button onClick={() => toggleSidebar(!sidebar)}>toggle sidebar</button>
    </nav>
  )
}

export default Header
