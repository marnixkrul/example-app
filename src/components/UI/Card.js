import React from 'react'

const Card = ({ image, title, text, children }) => {
  const style = {
    margin: '1rem 0',
  }
  const imageStyle = {
    maxHeight: '200px',
  }
  return (
    <div className="card" style={style}>
      {image ? (
        <img
          src={image}
          alt="card"
          style={imageStyle}
          className="card-img-top"
        />
      ) : null}
      <div className="card-body">
        <h3 className="card-title">{title} </h3>
        <p style={{ color: 'var(--text-grey)' }}>{text}</p>
        {children}
      </div>
      <div className="card-footer">
        <a href="#" className="btn btn-outline-primary">
          Dummy Button
        </a>
      </div>
    </div>
  )
}

export default Card
