import React from 'react'
import {
  FiHome,
  FiActivity,
  FiSettings,
  FiBook,
  FiBookOpen,
  FiUser,
} from 'react-icons/fi'
import { useLocation } from 'react-router-dom'
import './SideBar.css'

const SideBar = () => {
  const location = useLocation()
  const sidebarContent = [
    {
      icon: <FiHome />,
      text: 'Home',
      href: '/',
    },
    {
      icon: <FiActivity />,
      text: 'Activity',
      href: '/activity',
    },
    {
      icon: <FiBookOpen />,
      text: 'Notes',
      href: '/notes',
    },
    {
      icon: <FiUser />,
      text: 'Profile',
      href: '/profile',
    },
  ]
  const createItems = (items) => {
    return items.map((item, index) => (
      <li className="nav-item" key={index}>
        <a href={item.href}>
          <span
            className={`nav-link ${
              location.pathname === item.href ? 'active' : ''
            }`}
          >
            {item.icon} <span className="link-text">{item.text}</span>
          </span>
        </a>
      </li>
    ))
  }
  return (
    <nav className="sidebar">
      <ul className="sidebar-nav">{createItems(sidebarContent)}</ul>
    </nav>
  )
}

export default SideBar
