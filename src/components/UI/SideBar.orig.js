import React from 'react'
import { FiSettings, FiBookOpen, FiActivity, FiRepeat } from 'react-icons/fi'
import './SideBar.css'

const SideBar = ({ visible }) => {
  if (!visible) return null
  return (
    <nav className="sidebar">
      <ul className="sidebar-nav">
        <li className="logo nav-item">
          <a href="#" className="nav-link">
            <FiRepeat className="icon" />
            <span className="link-text logo-alt">Home </span>
          </a>
        </li>
        <li className="nav-item">
          <a href="#" className="nav-link">
            <FiActivity className="icon" />
            <span className="link-text">Activity</span>
          </a>
        </li>
        <li className="nav-item">
          <a href="#" className="nav-link">
            <FiBookOpen className="icon" />
            <span className="link-text">Notes</span>
          </a>
        </li>
        <li className="nav-item">
          <a href="#" className="nav-link">
            <FiSettings className="icon" />
            <span className="link-text">Settings</span>
          </a>
        </li>
      </ul>
    </nav>
  )
}

export default SideBar
