import React from 'react'
import { FiFile, FiUser, FiActivity, FiBookOpen } from 'react-icons/fi'
import Card from '../components/UI/Card'
import './Home.css'
import { Link } from 'react-router-dom'

const Home = () => {
  const cardList = (
    <ul className="cardlist">
      <li className="cardlist-item">
        <Link to="/activity">
          <FiActivity /> Activity
        </Link>
      </li>
      <li className="cardlist-item">
        <Link to="/notes">
          <FiBookOpen /> Bestanden
        </Link>
      </li>
      <li className="cardlist-item">
        <Link to="/profile">
          <FiUser /> Ga naar profiel
        </Link>
      </li>
    </ul>
  )
  const cards = [
    {
      title: 'Een kaartje',
      text:
        'Dit is een standaard voorbeeld kaart met wat informatie, Er staat vooral vullertekst om de layout te laten zien.',
    },
    {
      title: 'Kaart met extra children',
      text:
        'In dit voorbeeld worden children doorgegeven aan het card component.',
      children: cardList,
    },
    {
      title: 'Kaart met een afbeelding',
      text: 'in dit kaartje is een afbeelding meegegeven.',
      image:
        'https://images.pexels.com/photos/4389409/pexels-photo-4389409.jpeg?cs=srgb&dl=brown-brick-building-during-night-time-4389409.jpg&fm=jpg',
    },
  ]

  const renderRow = () => {
    console.log(cards)
    return cards.map((card) => (
      <div className="col-xs-12 col-md-4">
        <Card
          image={card.image}
          title={card.title}
          text={card.text}
          image={card.image}
        >
          {card.children}
        </Card>
      </div>
    ))
  }
  return (
    <>
      <div className="row col-xs-12">
        <h1>Home</h1>
      </div>
      <div className="row">{renderRow()}</div>
    </>
  )
}

export default Home
