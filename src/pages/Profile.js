import React, { useState, useEffect, Fragment } from 'react'
import { FiMail, FiPhone } from 'react-icons/fi'
import './Profile.css'

const Profile = () => {
  const [user, setUser] = useState()

  useEffect(() => {
    console.log('in effect')
    if (user) return
    fetch('https://randomuser.me/api/')
      .then((response) => response.json())
      .then(({ results }) => {
        console.log(results[0])
        setUser(results[0])
      })
  })

  const profilePicture = () => (
    <img className="profile-picture" src={user.picture.large} alt="profile" />
  )
  const userInfo = () => (
    <Fragment className="user">
      <h3>
        {`${user.name.first} ${user.name.last}, `}
        <small className="username">@{user.login.username}</small>
      </h3>
      <p>
        <FiMail className="contact-icon" /> {user.email} <br />
        <FiPhone className="contact-icon" /> {user.cell}
      </p>
    </Fragment>
  )
  const address = () => (
    <p>
      {`${user.location.street.name} ${user.location.street.number}`}
      <br />
      {`${user.location.postcode}, ${user.location.city} `}
      <br />
      {user.location.country}
    </p>
  )
  return user ? (
    <div className="row">
      <div className="col-xs-12">
        {profilePicture()}
        {userInfo()}
        {address()}
      </div>
    </div>
  ) : null
}
export default Profile
